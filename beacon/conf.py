CLI_CONFIG = {
    "acct_file": {"source": "acct", "os": "ACCT_FILE",},
    "acct_key": {"source": "acct", "os": "ACCT_KEY"},
    "format": {},
}
CONFIG = {
    "format": {
        "type": str,
        "help": "The plugin to use for processing data",
        "default": "json",
    }
}
DYNE = {
    "acct": ["acct"],
    "beacon": ["beacon"],
}
