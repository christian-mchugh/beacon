import asyncio
from typing import AsyncGenerator


def __init__(hub):
    hub.beacon.local.QUEUE = asyncio.Queue()


async def listen(hub) -> AsyncGenerator:
    """
    listen for data on the local queue
    """
    async for event in hub.beacon.local.channel():
        yield event


async def channel(hub) -> AsyncGenerator:
    while hub.beacon.RUN_FOREVER:
        yield await hub.beacon.local.QUEUE.get()

    raise StopAsyncIteration("No more messages in local queue")


async def publish(hub, data):
    """
    For testing, add something to the queue
    """
    await hub.beacon.local.QUEUE.put(data)
