import asyncio
import mock
import pytest
import uuid


@pytest.fixture(scope="function")
def hub(hub):
    hub.pop.sub.add(dyne_name="beacon")

    with mock.patch("sys.argv", ["beacon"]):
        hub.pop.config.load(["beacon"], cli="beacon")

    yield hub


@pytest.mark.asyncio
@pytest.fixture(scope="function")
async def beacon(hub):
    """
    A bare minimum beacon with only local queues and no outside connections
    """
    task: asyncio.Task = asyncio.create_task(
        hub.beacon.init.start(format_plugin="json")
    )

    while not hub.beacon.STARTED:
        await asyncio.sleep(0)

    yield

    with pytest.raises(StopAsyncIteration):
        hub.beacon.RUN_FOREVER = False
        hub.beacon.RUN_FOREVER = False
        await task


@pytest.fixture(scope="function")
def event(hub):
    return f"bacon-event-{uuid.uuid4()}"


@pytest.mark.asyncio
@pytest.fixture(scope="function")
async def rabbitmq_beacon(hub):
    """
    Start beacons with a rabbitmq connection
    """
    channel_name = f"bacon-test-{uuid.uuid4()}"

    # TODO get these by setting hub.OPT.acct.acct_file and hub.OPT.acct.acct_key from the cli for CI
    # Credentials for connecting to a local rabbitmq server
    hub.acct.PROFILES = {
        "pika": {
            "profile": {"host": "localhost", "port": 5672, "channels": [channel_name]}
        }
    }
    hub.acct.UNLOCKED = True

    task: asyncio.Task = asyncio.create_task(
        hub.beacon.init.start(format_plugin="json")
    )

    while not hub.beacon.STARTED:
        await asyncio.sleep(0)

    ctx = hub.acct.SUB_PROFILES.beacon.pika.profile

    if not ctx.connected:
        pytest.skip("No local rabbitmq server")

    yield ctx, channel_name

    # Cleanup
    channel = await ctx.connection.channel()
    await channel.queue_delete(channel_name)
    hub.acct.UNLOCKED = False
    hub.acct.SUB_PROFILES.clear()
    hub.acct.PROFILES.clear()

    # Stop beacons
    with pytest.raises(StopAsyncIteration):
        hub.beacon.RUN_FOREVER = False
        hub.beacon.RUN_FOREVER = False
        await task


@pytest.mark.asyncio
@pytest.fixture(scope="function")
async def postgresql_beacon(hub, postgresql):
    """
    Start beacons with a rabbitmq connection
    """
    # Credentials for connecting to a local PostgreSQL server
    channel = "test_postre"
    hub.acct.PROFILES = {
        "pg": {
            "profile": {
                "host": "localhost",
                "port": 5432,
                "password": "test_password",
                "user": "test_user",
                "database": "test_db",
                "channels": [channel],
            }
        }
    }
    hub.acct.UNLOCKED = True

    task: asyncio.Task = asyncio.create_task(
        hub.beacon.init.start(format_plugin="json")
    )

    while not hub.beacon.STARTED:
        await asyncio.sleep(0)
    ctx = hub.acct.SUB_PROFILES.beacon.pg.profile
    if not ctx.connected:
        pytest.skip("local PostreSQL could not connect")

    yield ctx, channel

    # Cleanup
    hub.acct.UNLOCKED = False
    hub.acct.SUB_PROFILES.clear()
    hub.acct.PROFILES.clear()

    # Stop beacons
    hub.beacon.RUN_FOREVER = False
    hub.beacon.RUN_FOREVER = False
    try:
        await task
    except StopAsyncIteration:
        ...
