import aio_pika
import asyncio
import pytest


@pytest.mark.asyncio
async def test_listen(hub, rabbitmq_beacon, event):
    # Get a configured channel from the connection, it should already be declared
    ctx, routing_key = rabbitmq_beacon

    # Let other processes wrap up before publishing
    await asyncio.sleep(0.1)

    # Publish an event to the queue
    channel: aio_pika.Channel = await ctx.connection.channel()
    await channel.default_exchange.publish(
        aio_pika.Message(event.encode()), routing_key=routing_key,
    )

    # Verify that it made it all the way through to post
    result = await hub.ingress.init.get()

    assert result.pop("timestamp")
    assert result["ref"] == "beacon.pika"
    assert event == result["data"]
