import pytest
import shutil


@pytest.mark.skipif(
    not all(shutil.which(pkg) for pkg in ("pg_ctl", "pg_config")),
    reason="Required libs not found",
)
@pytest.mark.asyncio
async def test_listen(hub, postgresql_beacon, postgresql, event):
    # Get a configured channel from the connection, it should already be declared
    ctx, channel = postgresql_beacon

    # TODO Publish a notification to the PostgreSQL
    pytest.skip("Don't know how to publish a notification to PostgreSQL yet")

    #async with ctx.connection.transaction():
    #    await ctx.connection.execute("CREATE TABLE mytable (a int)")
    #async with ctx.connection.transaction():
    #    await ctx.connection.execute("INSERT INTO mytable (a) VALUES (1)")

    # Verify that it made it all the way through to post
    result = await hub.ingress.init.get()

    assert result.pop("timestamp")
    assert result["ref"] == "beacon.pg"
    assert event == result["data"]
