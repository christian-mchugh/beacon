from typing import AsyncGenerator

import mock
import pytest


def test_cli(hub):
    with mock.patch("sys.argv", ["beacon"]):
        hub.beacon.RUN_FOREVER = False
        hub.beacon.RUN_FOREVER = False
        with pytest.raises(StopAsyncIteration):
            hub.beacon.init.cli()


@pytest.mark.asyncio
async def test_start(hub):
    hub.beacon.RUN_FOREVER = False
    hub.beacon.RUN_FOREVER = False
    with pytest.raises(StopAsyncIteration):
        await hub.beacon.init.start("raw")


@pytest.mark.asyncio
async def test_listeners(hub):
    listeners = hub.beacon.init.listeners({})
    assert all(isinstance(listen, AsyncGenerator) for listen in listeners)


@pytest.mark.asyncio
async def test_listen(hub):
    hub.beacon.RUN_FOREVER = False
    async for event, ref in hub.beacon.init.listen():
        assert ref == "beacon.init"
        assert event is hub.beacon.init.STOP_ITERATION
