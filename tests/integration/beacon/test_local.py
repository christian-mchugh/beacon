import pytest


@pytest.mark.asyncio
async def test_listen(hub, beacon, event):
    """
    Test the full path of an event, from ingress to ingress
    """
    # Put an event on the queue
    await hub.beacon.local.publish(event)

    # Verify that it made it all the way through to post
    result = await hub.ingress.init.get()

    assert result["timestamp"]
    assert result["ref"] == "beacon.local"
    assert event == result["data"]
